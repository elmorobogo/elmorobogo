# README #

This is the 'Team ElMo PiWars' codebase (see www.piwars.org).  Created so that the judges can see the quality of our code.

### What is this repository for? ###

* the code here is designed specifically for our robot.  I don't imagine it could have a general use for others, beyond browsing the source
* the elmo.py file will initialise ElMo, our robot, and start a menu that allows us to dispatch the appropriate method for each task
* I'm a git newbie, so some of the commits might seem odd :)
* UltraBorg and PicoBorgReverse files are courtesy of the piborg website