#!/usr/bin/env python
# coding: Latin-1
'''
   ElMo code for the PiWars robotic challenge
   
the 'main' function at the bottom of this script creates an ElMo object
all other tasks are ElMo methods e.g

   elmo = ElMo()
   elmo.straightlinespeed()
   elmo.threepointturn()
   elmo.proximityalert()

the menu method, called in the main function, allows us to scroll through
these various methods and select the one we wish to execute

'''

import ConfigParser
import os
import pygame
import time
import sys

import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)

# packages for control of the OLED display
import Adafruit_SSD1306
import Image
import ImageDraw
import ImageFont

# PicoBorgRev is used to control the motors - see 
#    https://www.piborg.org/picoborgrev
import PicoBorgRev

# Ultraborg is used to control the SR04 ultrasonic sensors - see
#    https://www.piborg.org/ultraborg
import UltraBorg

#this is where the image, font and config files reside
INSTALL_DIR = '/home/pi/elmorobogo'

class ElMo(object):
    '''ElMo is the object that controls our robot'''
    def __init__(self):
        '''initialises ElMo the robot'''
#set up the motor controller
        self.pbr = PicoBorgRev.PicoBorgRev()
        self.pbr.Init()
#disable the communications failsafe
        self.pbr.SetCommsFailsafe(False)
#reset the emergency power off
        self.pbr.ResetEpo()

#set up the ultrasonic controller
        self.ub = UltraBorg.UltraBorg()
        self.ub.Init()

#config file settings
        self.configfile = os.path.join(INSTALL_DIR, 'elmo.cfg')
        self.config = ConfigParser.RawConfigParser()
        self.config.read(self.configfile)
        self.voltagein = self.config.getfloat('ElMo', 'voltagein')
#our motors are rated for 6V - but we can run them at 9V for a short period
	self.voltageout = self.config.getfloat('ElMo', 'voltageout')
#so if voltagein is eg 12V, we set maxpower to 0.5 to protect the motors
        self.maxpower = self.voltageout/self.voltagein
#we can ramp up the 'slow' speed in the proximity test - sometimes 0.25
#is not enough to move ElMo, so it is worth upping this to 0.33
	self.slowprox = self.config.getfloat('ElMo', 'slowprox')
#time to travel 1 metre
        self.time1m = self.config.getfloat('ElMo', 'time1m')
#time to rotate 360 degrees
        self.time360 = self.config.getfloat('ElMo', 'time360')
#speed for line following - slow and sure seems to be the way to go (0.4)
	self.linefollowspeed = self.config.getfloat('ElMo', 'linefollowspeed')

#set up the oled display - I'm not sure what rst=24 is actually for 
#but it's in the example, so leave it in for now
        self.disp = Adafruit_SSD1306.SSD1306_128_64(rst=24, i2c_address=0x3C)
        self.disp.begin()
        self.fontsize = 24
        fontfile = os.path.join(INSTALL_DIR, 'font/Mario-Kart-DS.ttf')
        self.font = ImageFont.truetype(fontfile, self.fontsize)
        self.image = Image.new('1', (128, 64))
        self.draw = ImageDraw.Draw(self.image)

#set up the IR line following sensors
        self.gpio_sensor_left = 10
        self.gpio_sensor_centre = 9
        self.gpio_sensor_right = 11
        GPIO.setup(self.gpio_sensor_left, GPIO.IN)
        GPIO.setup(self.gpio_sensor_centre, GPIO.IN)
        GPIO.setup(self.gpio_sensor_right, GPIO.IN)

#set up the menu buttons
        self.gpio_button_left  = 26
        self.gpio_button_centre  = 19
        self.gpio_button_right  = 13
        GPIO.setup(self.gpio_button_left, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.setup(self.gpio_button_centre, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.setup(self.gpio_button_right, GPIO.IN, pull_up_down=GPIO.PUD_UP)

#set up the cannon firing pin
        self.gpio_cannon = 21
        GPIO.setup(self.gpio_cannon, GPIO.OUT)

#self.item keeps track of the currently selected menu item
        self.item = 0

    @property
    def button_left(self):
        '''return true if left button is pressed'''
#do I need to worry about debouncing? doesn't seem to be an issue
        return not GPIO.input(self.gpio_button_left)

    @property
    def button_centre(self):
        '''return true if centre button is pressed'''
#do I need to worry about debouncing? doesn't seem to be an issue
        return not GPIO.input(self.gpio_button_centre)

    @property
    def button_right(self):
        '''return true if right button is pressed'''
#do I need to worry about debouncing? doesn't seem to be an issue
        return not GPIO.input(self.gpio_button_right)

    @property
    def sensor_left(self):
        '''return the value of the left sensor (True/Fase)'''
        return GPIO.input(self.gpio_sensor_left)

    @property
    def sensor_centre(self):
        '''return the value of the centre sensor (True/Fase)'''
        return GPIO.input(self.gpio_sensor_centre)

    @property
    def sensor_right(self):
        '''return the value of the right sensor (True/Fase)'''
        return GPIO.input(self.gpio_sensor_right)

    def writeconfig(self):
        f = open(self.configfile, 'wb')
        self.config.write(f)
        f.close()
	self.display('saved')
	time.sleep(1.0)

    def setvoltagein(self):
        self.voltagein = self.changevalue(self.voltagein, 'voltagein', 0.1)
        self.maxpower = self.voltageout/self.voltagein
        self.config.set('ElMo', 'voltagein', '%.1f'%self.voltagein)
	self.writeconfig()

    def setvoltageout(self):
        self.voltageout = self.changevalue(self.voltageout, 'voltageout', 1.0)
        self.maxpower = self.voltageout/self.voltagein
        self.config.set('ElMo', 'voltageout', '%.1f'%self.voltageout)
	self.writeconfig()

    def setslowprox(self):
        self.slowprox = self.changevalue(self.slowprox, 'slowprox', 0.01)
        self.config.set('ElMo', 'slowprox', '%.2f'%self.slowprox)
	self.writeconfig()

    def settime1m(self):
        self.time1m = self.changevalue(self.time1m, 'time1m', 0.1)
        self.config.set('ElMo', 'time1m', '%.1f'%self.time1m)
	self.writeconfig()

    def settime360(self):
        self.time360 = self.changevalue(self.time360, 'time360', 0.1)
        self.config.set('ElMo', 'time360', '%.1f'%self.time360)
	self.writeconfig()

    def setlinefollowspeed(self):
        self.linefollowspeed = self.changevalue(self.linefollowspeed,
            'lfspeed', 0.05)
        self.config.set('ElMo', 'linefollowspeed', '%.2f'%self.linefollowspeed)
	self.writeconfig()

    def changevalue(self, val, label, increment):
	while 1:
            self.display('%s %.2f'%(label, val))
            if self.button_centre:
		return val
            elif self.button_left:
                val -= increment
            elif self.button_right:
                val += increment
            time.sleep(0.1)

    def menu(self):
        '''runs the ElMo menu - scroll through the menu items using the left
and right buttons.  use the centre button to perform an action'''
        items = [
            ('set battery voltage', self.setvoltagein),
            ('set lfspeed', self.setlinefollowspeed),
            ('line follow', self.linefollower),
            ('set motor voltage', self.setvoltageout),
            ('straight line speed', self.straightlinespeed),
            ('set slowprox', self.setslowprox),
            ('proximity alert', self.proximityalert),
            ('calibrate 1metre', self.calibrate1m),
            ('set time1m', self.settime1m),
            ('calibrate 360degrees', self.calibrate360),
            ('set time360', self.settime360),
            ('3 point turn', self.threepointturn),
            ('connect ps3', self.connectps3),
            ('obstacle course', self.remote),
            ('pinoon', self.remote),
            ('fire cannon', self.cannontest),
            ('skittles', self.remotewithcannon),
            ('shutdown pi', self.shutdown),
            ('quit menu', self.quit),
        ]
        nitems = len(items)
        prev = -1
        while 1:
            if self.button_left:
                self.item -= 1
                if self.item < 0:
                    self.item = nitems - 1
                time.sleep(0.1)
            elif self.button_right:
                self.item += 1
                if self.item > nitems - 1:
                    self.item = 0
                time.sleep(0.1)
            elif self.button_centre:
                label, func = items[self.item]
#we clicked the button - perform the selected task
                func()
                self.display(label)
            if self.item != prev:
                prev = self.item
                label, func = items[self.item]
                self.display(label)

    def splash(self, nseconds=5):
        '''display a splash screen on the oled display'''
        splashfile = os.path.join(INSTALL_DIR, 'images/elmo_splashscreen.ppm')
        image = Image.open(splashfile).convert('1')
        self.disp.image(image)
        self.disp.display()
        time.sleep(nseconds)

    def display(self, msg):
        '''write msg to the oled display, one word per line'''
        self.disp.clear()
        self.draw.rectangle(((0, 0), (127, 63)), fill=0)
        yval = 2
        for word in msg.split():
            self.draw.text((2, yval), word, font=self.font, fill=255)
            yval += self.fontsize -3 # offset based on size of font
        self.disp.image(self.image)
        self.disp.display()

    def cleardisplay(self):
        '''clear the oled display'''
        self.disp.clear()
        self.disp.display()

    def countdown(self, delay=0.5):
        '''display a ready, steady, go countdown on the oled screen.
wait delay seconds between each message'''
        for c in ('ready', 'steady'):
            self.display(c)
            time.sleep(delay)
        self.display('GO!')

    def setmotors(self, power):
        '''set motors to power'''
        self.pbr.SetMotor1(power)
        self.pbr.SetMotor2(-power)

    def forward(self, distance):
        '''travel forward for distance metres'''
        onfor = distance*self.time1m
        self.setmotors(self.maxpower)
        time.sleep(onfor)
        self.pbr.MotorsOff()

    def backward(self, distance):
        '''travel backward for distance metres'''
        onfor = distance*self.time1m
        self.setmotors(-self.maxpower)
        time.sleep(onfor)
        self.pbr.MotorsOff()

    def clockwise(self, angle):
        '''rotate clockwise for angle degrees'''
        onfor = (angle/360.0)*self.time360
        self.pbr.SetMotor1(-self.maxpower)
        self.pbr.SetMotor2(-self.maxpower)
        time.sleep(onfor)
        self.pbr.MotorsOff()

    def counterclockwise(self, angle):
        '''rotate counterclockwise for angle degrees'''
        onfor = (angle/360.0)*self.time360
        self.pbr.SetMotor1(self.maxpower)
        self.pbr.SetMotor2(self.maxpower)
        time.sleep(onfor)
        self.pbr.MotorsOff()

    def connectps3(self):
        '''start the sixad daemon - then press the ps3 button to connect the
controller.  See http://qtsixa.sourceforge.net/'''
        os.system('sixad --start &')

    def cannon(self):
        '''triggers the cannon - see https://tris10.com/product/kickball-kit'''
        GPIO.output(self.gpio_cannon, GPIO.HIGH)
        time.sleep(0.080)
        GPIO.output(self.gpio_cannon, GPIO.LOW)

    def cannontest(self):
        '''wrapper to test the cannon'''
        self.countdown()
        self.cannon()

    def finish(self):
        '''tidy up - clear the display, stop the motors, cleanup GPIO'''
        self.pbr.MotorsOff()
        self.cleardisplay()
        GPIO.cleanup()

    def quit(self):
        '''tidy up and exit'''
        self.finish()
        sys.exit()

    def shutdown(self):
        '''tidy up and shut down the raspberry pi'''
        self.finish()
        os.system('shutdown -h now')

    def calibrate1m(self):
        '''drive forward '1m' - assuming self.time1m is correct'''
        self.countdown()
        self.forward(1.0)

    def calibrate360(self):
        '''rotate 360 degrees - assuming self.time360 is correct'''
        self.countdown()
        self.clockwise(360.0)

    def threepointturn(self):
        '''perform the 3 point turn challenge. this just uses distances.
I hope our maths is correct!  Note that ElMo is 24cm long'''
        self.countdown()
        self.forward(1.70)
        self.counterclockwise(90)
        self.forward(0.34)
        self.backward(0.68)
        self.forward(0.34)
        self.counterclockwise(90)
        self.forward(1.70)
        return

    def proximityalert(self):
        '''perform proximity test - stop when we are within 40mm of the wall'''

        slow_mm = 250 # slow down when we are 250mm from the wall
        stop_mm = 40 # stop when we are 40mm from the wall
        self.countdown() # ready..steady..go
        power = self.maxpower
        slow = self.maxpower * self.slowprox
        self.setmotors(power)
        while(1):
            if self.button_centre:
                self.pbr.MotorsOff()
                return

            u1 = self.ub.GetDistance1()
            u2 = self.ub.GetDistance2()

            if u1 < stop_mm or u2 < stop_mm:
                self.pbr.MotorsOff()
                return

            if u1 < slow_mm and power != slow:
                power = slow
                self.setmotors(power)

            time.sleep(0.1)

    def error_linefollower(self, reference, last_error):
        '''return a value between -2 and +2 depending on the IR sensor values'''
        l = self.sensor_left
        c = self.sensor_centre
        r = self.sensor_right
        if l and c:
            error = -1.0
        elif c and r:
            error = 1.0
        elif l:
            error = -2.0
        elif c:
            error = 0.0
        elif r:
            error = 2.0
        else:
#for any other state, return the same error as last time
            error = last_error
        return error

    def error_straightlinespeed(self, reference, last_error):
        '''return the difference between the current distance from the wall and
the reference difference from the wall'''
        u1 = self.ub.GetDistance1()
        error = u1 - reference
        return error

    def start_linefollower(self):
        '''the line follower does not need a refererence state, so this function
does nothing'''
        return None

    def start_straightlinespeed(self):
        '''returns the distance from the ultrasonic sensor to the wall'''
#we only use the sensor on the LHS
        references = []
        for i in range(5):
            references.append(self.ub.GetDistance1())
        references.sort()
        return references[2] # take median of 5 readings

    def linefollower(self):
        '''perform the line follower.  uses the IR sensors to check and correct
distance from the black line'''
        error_function = self.error_linefollower
        start_function = self.start_linefollower
        self.pid(start_function, error_function,
            maxticks=36000, # will run for 1 hour?!
            update=0.1,
            startpower=self.linefollowspeed,
            p_coeff=-0.2,
            d_coeff=0.1
        )

    def straightlinespeed(self):
        '''perform the straight line speed test.  uses the lhs ultrasonic sensor
to check and correct distance from the wall'''
        error_function = self.error_straightlinespeed
        start_function = self.start_straightlinespeed
        self.pid(start_function, error_function,
            maxticks=100, # stop after update*maxticks seconds
            update=1.0,
            startpower=0.95,
            p_coeff=0.01,
            d_coeff=0.01
        )

    def pid(self, start_function, error_function, maxticks=30, update=1.0,
        startpower=0.95, p_coeff=0.01, d_coeff=0.005):
        '''this code uses a proportional-integral-derivative controller(PID
        controller) to keep elmo on track - the same code can be used for both
        the straight line speed test and the line follower - you just need to
        provide a different error function (distance from the wall or distance
        from the black line).  See eg
            https://en.wikipedia.org/wiki/PID_controller 
            http://www.societyofrobots.com/member_tutorials/book/export/html/350
        for more of the theory

        The other parameters are also tuned for each task:

        maxticks stops the function after N ticks
        update is the time in seconds to wait after each tick
        startpower is the motor speed at the start
        p_coeff is the proportional coefficient
        d_coeff is the derivative coefficient
        i_coeff is not used yet (so we're actually using pd control)'''

        maxcorrection = 1.0 - startpower # clamp the correction in motor speed
        self.countdown() # ready, steady, go
        reference = start_function()
        running = True
        self.pbr.SetMotor1(self.maxpower*startpower)
        self.pbr.SetMotor2(-self.maxpower*startpower)
        ticks = 0 # counter for stopping after N steps
        lasterror = 0.0
        while running:
            if ticks > maxticks or self.button_centre:
                running = False
            error = error_function(reference, lasterror)
            p = error * p_coeff
            i = 0.0
#           dval = error - lasterror
            d = (error - lasterror) * d_coeff
            lasterror = error
            correction = p + i + d
            if correction > 0:
                correction = min(maxcorrection, correction)
            else:
                correction = max(-maxcorrection, correction)
            newpower1 = startpower + correction
            newpower2 = startpower - correction
#           print u1, error, newpower1, newpower2, dval, 
#               p, d, p+d, newpower1, newpower2
            self.pbr.SetMotor1(self.maxpower * newpower1)
            self.pbr.SetMotor2(-self.maxpower * newpower2)
            time.sleep(update)
            ticks += 1
        self.pbr.MotorsOff()
#put in a delay (4 seconds) so we don't inadvertently restart the program
#because we hold down the centre button for too long
        self.display('done')
        time.sleep(4.0)

    def remote(self, do_cannon=False):
        '''use the ps3 remote to drive ElMo.  assumes the joystick is paired.
        Note that we drive ElMo 'backwards' compared to other tasks
        this code is a stripped back and modified version of the diddyJoy.py
        script provided by PiBorg

        set do_cannon=True to enable firing the cannon when you press the triangle key
        '''
#all this boilerplate should perhaps go in a remoteoptions object?
        axisupdown = 1      # Joystick axis to read for up / down position
        axisleftright = 2   # Joystick axis to read for left / right position
        buttonslow = 8      # Joystick button number for driving slowly (L2)
        buttonquit = 3      # Joystick button number to exit (start)
        buttoncannon = 12   # Joystick button number to fire cannon (triangle)
        buttonfastturn = 9  # Joystick button number for turning fast (R2)
        slowfactor = 0.5    # Speed to slow to when drive slowly button is held
        interval = 0.00     # Time between updates in seconds
#motors shouldn't be on anyway?
        self.pbr.MotorsOff()
        os.environ["SDL_VIDEODRIVER"] = "dummy" # Removes need for GUI window
#not sure what happens if we try and run this code twice in the same session?
        pygame.init()
        pygame.display.set_mode((1, 1))
#not sure what happens if we try and run this code twice in the same session?
        pygame.joystick.init()
        joystick = pygame.joystick.Joystick(0)
        joystick.init()

#if I restart the remote, someties the cannon fires immediately
#maybe there arestill events on the stack?  
#lets try clearing before we start...
        pygame.event.clear()

        driveleft = 0.0
        driveright = 0.0
        running = True
        hadevent = False
        updown = 0.0
        leftright = 0.0
        last_fired = 0.0
# Loop indefinitely - until the cannon is fired or the quit button is pressed
        while running:
# Get the latest events from the system
            hadevent = False
            events = pygame.event.get()
# Handle each event individually
            for event in events:
                if event.type == pygame.JOYBUTTONDOWN:
# A button on the joystick just got pushed down
                    hadevent = True
                elif event.type == pygame.JOYAXISMOTION:
# A joystick has been moved
                    hadevent = True

                if hadevent:
#pressed the pause button - this takes us out of the loop and back to the menu
                    if joystick.get_button(buttonquit):
                        running = False
#pressed the cannon button - fire cannon and return to the menu
                    if do_cannon and joystick.get_button(buttoncannon):
                        self.pbr.MotorsOff()
                        current_time = time.time()
                        if current_time - last_fired > 10.0:
                            self.cannon()
                            last_fired = current_time
#remove all events that are in the queue
                            pygame.event.clear()
                            time.sleep(1.0)
# Read axis positions (-1 to +1) - our axes are inverted
                    updown = -joystick.get_axis(axisupdown)
                    leftright = -joystick.get_axis(axisleftright)
# Apply steering speeds
                    if not joystick.get_button(buttonfastturn):
                        leftright *= 0.5
# Determine the drive power levels - should we cancel these -ve signs?
                    driveleft = -updown
                    driveright = -updown

                    if leftright < -0.05:
# Turning left
                        driveleft *= 1.0 + (2.0 * leftright)
                    elif leftright > 0.05:
# Turning right
                        driveright *= 1.0 - (2.0 * leftright)
                    if joystick.get_button(buttonslow):
                        driveleft *= slowfactor
                        driveright *= slowfactor
# Set the motors to the new speeds
                    self.pbr.SetMotor1(driveright * self.maxpower)
                    self.pbr.SetMotor2(-driveleft * self.maxpower)
# Wait for the interval period
            time.sleep(interval)
# Disable all drives
        self.pbr.MotorsOff()

    def remotewithcannon(self):
        '''call the remote control method, PLUS allow a user to
           fire the cannon.  The PS3 controller must be enabled'''
        self.remote(do_cannon=True)

def main():
    '''this is the main routine
       it creates an ElMo object and starts the menu system'''
    try:
        elmo = ElMo() # initialise the robot
        elmo.splash(3) # show the elmo splash screen for 2 seconds
        elmo.menu() # run the menu method
    except KeyboardInterrupt:
        elmo.finish()

if __name__ == "__main__":
    main()
